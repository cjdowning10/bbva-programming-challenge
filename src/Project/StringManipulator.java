package Project;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/*
 * Name: Corey Downing
 * Last Modified: 01/17/2018
 * 
 * Version 1.0
 */

// Private Variables and Constructor //
//----------------------------------------------------------------------------------------------------//
public class StringManipulator
{
	
	String word;
	double weight;
	private static int heapSize;
	
	/*
	 * Used primarily to sort words in an array using their weights to compare to each other.
	 */
	public StringManipulator(String word, double weight)
	{
		this.word = word;
		this.weight = weight;
	}
	
	private double returnWeight()
	{
		return weight;
	}
	
	private String returnWord()
	{
		return word;
	}
	
// Main Method //
//----------------------------------------------------------------------------------------------------//
	
	/*
	 * The String values of the file "challenge_tests.csv" is into an array. This array is then sorted
	 * using a Max Heap Sort algorithm, comparing the words in the file based on their relative 'weight'.
	 * The resulting words in the sorted array are then normalized and written to "challenge_sorted.csv",
	 * along with their weight and whether they have unique characters.
	 */
	public static void main(String[] args) throws IOException
	{
		BufferedReader input = new BufferedReader(new FileReader("src/atdd/challenge_tests.csv"));
		BufferedWriter output = new BufferedWriter(new FileWriter("src/atdd/challenge_sorted.csv"));
		ArrayList<String> wordList = new ArrayList<String>();
		String line = input.readLine();
		String normalizedWord;
		String isUnique;
		
		// Read the input file and set up the output file to be written
		while ((line = input.readLine()) != null)
		{
		       wordList.add(line);
		}
		input.close();
		output.write("Words,Unique,Weight");
		output.newLine();
		
		// The list of words are put into an array for the sorting algorithm
		String[] array = new String[wordList.size()];
		array = wordList.toArray(array);
		StringManipulator[] sortedWords = sortStrings(array);
		
		// Normalize the words, find out if they have unique characters, and write the results
		for (StringManipulator word : sortedWords)
		{
			normalizedWord = cleanString(word.returnWord()) + ",";
			
			if (hasUniqueChars(normalizedWord))
			{
				isUnique = "TRUE,";
			}
			else
			{
				isUnique = "FALSE,";
			}
			
			output.write(normalizedWord + isUnique + word.returnWeight());
			output.newLine();
		}
		
		output.close();
	}
	
// Class Methods //
//----------------------------------------------------------------------------------------------------//
	
	/*
	 * Takes a String and returns a String with the non-alphabetic characters removed and 
	 * the remaining alphabetic characters set to upper case
	 */
	public static String cleanString(String string)
	{
		// Use the letter value of each character of the String and decide if they are a letter
		String cleanedString = "";
		char letterValue;
		
		for (int i = 0; i < string.length(); i++)
		{
			letterValue = string.charAt(i);
			
			// If the character is a letter, add it to the cleanedString, and make it upper case
			// if it is a lower case letter
			if (Character.isLetter(letterValue))
			{
				if (Character.isLowerCase(letterValue))
				{
					letterValue -= 32;
				}
				cleanedString += (char) letterValue;
			}
		}
		return cleanedString;
	}
	
	
	/*
	 * Return 'true' if the String has no repeats of alphabetic characters [a-z,A-Z], and
	 * return 'false' if it does repeat. A hash map is used to determine if the character
	 * has been seen before. It is not case-sensitive
	 */
	public static boolean hasUniqueChars(String word)
	{
		boolean result = true;
		char letterValue;
		HashMap<Character, Integer> map = new HashMap<Character, Integer>(26);
		
		for (int i = 0; i < word.length(); i++)
		{
			letterValue = Character.toUpperCase(word.charAt(i));
			if (Character.isLetter(letterValue))
			{
				if (!map.containsKey(letterValue))
				{
					map.put(letterValue, 0);
				}
				else
				{
					result = false;
					break;
				}
			}
		}
		return result;
	}
	
	
	/*
	 * Returns the average of each alphabetic character in the String [a-z, A-Z]. The 
	 * characters are not case-sensitive, so the value will be determined based on the
	 * upper case value of the character [A-Z]
	 * 
	 * The weight is a numeric value that is the average of each ASCII character value
	 * contained in the String
	 */
	public static double getWeight(String word)
	{
		double result = 0;
		double numOfLetters = 0;
		char letterValue;
		
		for (int i = 0; i < word.length(); i++)
		{
			letterValue = Character.toUpperCase(word.charAt(i));
			if (Character.isLetter(letterValue))
			{
				numOfLetters += 1;
				result += letterValue;
			}
		}
		
		if (numOfLetters != 0)
		{
			return (result / numOfLetters);
		}
		return 0;
	}
	
	/*
	 * A sorting algorithm that uses a Max Heap Sort to sort an array of Strings.
	 * A new array of type StringManipulator is created to be used by the heap to
	 * sort the Strings. The Strings are sorted based on their calculated weight
	 */
	public static StringManipulator[] sortStrings(String[] words)
	{
		StringManipulator[] array = new StringManipulator[words.length];
		for (int i = 0; i < array.length; i++)
		{
			String word = words[i];
			array[i] = new StringManipulator(word, getWeight(word));
		}
		
		// Create the heap and start sorting
		buildHeap(array);
		for (int i = heapSize; i > 0; i--)
		{
			swap(array, 0, i);
			heapSize -= 1;
			maxHeapify(array, 0);
		}
		
		return array;
	}
	
	/*
	 * Initializes the heap's size and starts sorting the array. Used by
	 * the sortStrings method
	 */
	private static void buildHeap(StringManipulator[] array)
	{
		heapSize = array.length - 1;
		for (int i = heapSize / 2; i >= 0; i--)
		{
			maxHeapify(array, i);
		}
	}
	
	/*
	 * Uses a Max Heap Sort algorithm to sort an array. The algorithm tests whether
	 * a node or its children have a larger weight. If the current node does not have
	 * the largest weight, it is swapped with the larger child and that child is then
	 * checked and sorted
	 */
	private static void maxHeapify(StringManipulator[] array, int i)
	{
		int leftChild = 2*i;
		int rightChild = 2*i + 1;
		int maxWeight = i;
		
		if (leftChild <= heapSize && array[leftChild].returnWeight() > array[i].returnWeight())
		{
			maxWeight = leftChild;
		}
		
		if (rightChild <= heapSize && array[rightChild].returnWeight() > array[maxWeight].returnWeight())
		{
			maxWeight = rightChild;
		}
		
		if (maxWeight != i)
		{
			swap(array, i, maxWeight);
			maxHeapify(array, maxWeight);
		}
	}
	
	/*
	 * Swaps two elements in an array 
	 */
	private static void swap(StringManipulator[] array, int i, int j)
	{
		StringManipulator temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}
